import babel from 'rollup-plugin-babel'

export default {
  input: './src/VuePopper.js',
  output: {
    file: 'vue-popper.js',
    format: 'cjs'
  },
  external: [
    'lodash/isEqual',
    'lodash/merge',
    'popper.js',
    'portal-vue'
  ],
  plugins: [
    babel({
      exclude: 'node_modules/**'
    })
  ]
}
