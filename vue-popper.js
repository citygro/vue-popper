'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var Popper = _interopDefault(require('popper.js'));
var isEqual = _interopDefault(require('lodash/isEqual'));
var merge = _interopDefault(require('lodash/merge'));
var portalVue = require('portal-vue');

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var PopperComponent = (function (Vue, defaults) {
  return Vue.component('popper', {
    render: function render() {
      var h = arguments[0];

      return h(
        'span',
        { 'class': 'cg-popper-target', ref: 'target' },
        [h(
          portalVue.Portal,
          {
            attrs: {
              tag: 'span',
              to: 'cg-popper-portal'
            }
          },
          [h(
            'span',
            {
              key: this._uid * this.$parent._uid,
              'class': 'cg-popper-ref',
              ref: 'popper'
            },
            [this.$slots.default]
          )]
        )]
      );
    },

    props: {
      options: {
        type: Object
      },
      target: {
        required: false
      }
    },
    data: function data() {
      return {
        findElementInterval: null
      };
    },
    mounted: function mounted() {
      this.createPopper();
    },
    updated: function updated() {
      this.updatePopper();
    },
    activated: function activated() {
      this.updatePopper({ replace: true });
    },
    beforeDestroy: function beforeDestroy() {
      if (this.popper) {
        this.popper.destroy();
      }
    },

    watch: {
      options: function options(newValue, oldValue) {
        if (!isEqual(newValue, oldValue)) {
          this.createPopper({ replace: true });
        }
      }
    },
    methods: {
      updatePopper: function updatePopper() {
        var _this = this;

        if (this.popper) {
          this.$nextTick(function () {
            return _this.popper.update();
          });
        } else {
          this.createPopper();
        }
      },
      createPopper: function createPopper() {
        var _this2 = this;

        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

        var replace = options.replace || false;
        if (!this.popper || replace) {
          var _options = merge({}, defaults, this.options, {
            removeOnDestroy: true
          });
          this.$nextTick(_asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var popper, target;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return _this2.findElement();

                  case 2:
                    popper = _context.sent;
                    target = _this2.target || _this2.$refs.target;

                    if (popper && target) {
                      try {
                        if (_this2.popper && replace) {
                          _this2.popper.destroy();
                        }
                        if (!_this2.popper) {
                          _this2.popper = new Popper(target, popper, _options);
                        }
                      } catch (err) {
                        console.error(err);
                      }
                    }

                  case 5:
                  case 'end':
                    return _context.stop();
                }
              }
            }, _callee, _this2);
          })));
        }
      },
      findElement: function findElement() {
        var _this3 = this;

        if (this.findElementInterval === null) {
          this.findElementInterval = setInterval(function () {
            var popper = _this3.$refs.popper;
            if (popper) {
              clearInterval(_this3.findElementInterval);
              _this3.findElementInterval = null;
              _this3.$emit('found-element', popper);
            }
          }, 100);
        }
        return new Promise(function (resolve) {
          var popper = _this3.$refs.popper;
          if (popper) {
            resolve(popper);
          } else {
            var handler = function handler(popper) {
              _this3.$off('found-element', handler);
              resolve(popper);
            };
            _this3.$on('found-element', handler);
          }
        });
      }
    }
  });
});

var VuePopper = {
  install: function install(Vue) {
    var defaults = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var h = this.$createElement;

    Vue.component('popper-portal', {
      render: function render() {
        var h = arguments[0];

        return h(portalVue.PortalTarget, {
          attrs: {
            multiple: true,
            name: 'cg-popper-portal'
          }
        });
      }
    });

    Vue.component('popper', PopperComponent(Vue, defaults));
  }
};

module.exports = VuePopper;
