import Popper from 'popper.js'
import isEqual from 'lodash/isEqual'
import merge from 'lodash/merge'
import {Portal} from 'portal-vue' // eslint-disable-line no-unused-vars

export default (Vue, defaults) => Vue.component('popper', {
  render () {
    return (
      <span class='cg-popper-target' ref='target'>
        <Portal
          tag='span'
          to='cg-popper-portal'
        >
          <span
            key={this._uid * this.$parent._uid}
            class='cg-popper-ref'
            ref='popper'
          >
            {this.$slots.default}
          </span>
        </Portal>
      </span>
    )
  },
  props: {
    options: {
      type: Object
    },
    target: {
      required: false
    }
  },
  data () {
    return {
      findElementInterval: null
    }
  },
  mounted () {
    this.createPopper()
  },
  updated () {
    this.updatePopper()
  },
  activated () {
    this.updatePopper({replace: true})
  },
  beforeDestroy () {
    if (this.popper) {
      this.popper.destroy()
    }
  },
  watch: {
    options (newValue, oldValue) {
      if (!isEqual(newValue, oldValue)) {
        this.createPopper({replace: true})
      }
    }
  },
  methods: {
    updatePopper () {
      if (this.popper) {
        this.$nextTick(() => this.popper.update())
      } else {
        this.createPopper()
      }
    },
    createPopper (options = {}) {
      const replace = options.replace || false
      if (!this.popper || replace) {
        const options = merge(
          {},
          defaults,
          this.options,
          {
            removeOnDestroy: true
          }
        )
        this.$nextTick(async () => {
          const popper = await this.findElement()
          const target = this.target || this.$refs.target
          if (popper && target) {
            try {
              if (this.popper && replace) {
                this.popper.destroy()
              }
              if (!this.popper) {
                this.popper = new Popper(target, popper, options)
              }
            } catch (err) {
              console.error(err)
            }
          }
        })
      }
    },
    findElement () {
      if (this.findElementInterval === null) {
        this.findElementInterval = setInterval(() => {
          const popper = this.$refs.popper
          if (popper) {
            clearInterval(this.findElementInterval)
            this.findElementInterval = null
            this.$emit('found-element', popper)
          }
        }, 100)
      }
      return new Promise((resolve) => {
        const popper = this.$refs.popper
        if (popper) {
          resolve(popper)
        } else {
          const handler = (popper) => {
            this.$off('found-element', handler)
            resolve(popper)
          }
          this.$on('found-element', handler)
        }
      })
    }
  }
})
