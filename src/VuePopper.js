import PopperComponent from './PopperComponent'
import {PortalTarget} from 'portal-vue' // eslint-disable-line no-unused-vars

export default {
  install (Vue, defaults = {}) {
    Vue.component('popper-portal', {
      render () {
        return (
          <PortalTarget
            multiple
            name='cg-popper-portal'
          />
        )
      }
    })

    Vue.component('popper', PopperComponent(Vue, defaults))
  }
}
