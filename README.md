# @citygro/vue-popper

VueJS popover component based on popper.js

## installation

```bash
yarn add @citygro/vue-popper
npm install @citygro/vue-popper
```

```javascript
import VuePopper from '@citygro/vue-popper'

Vue.use(VuePopper, {
  // default options
  // see <https://popper.js.org/popper-documentation.html#Popper>
})

// ...

const app = new Vue({
  render () {
    return (
      <div id='myApp'>
        <App />
        {/* required: allows poppers to hover above and outside any parent element */}
        <popper-portal />
      </div>
    )
  },
  // ...
})
```

## usage

```vue
<template>
  <a
    class="help-bubble"
    @mouseenter="handleEnter"
    @mouseleave="handleLeave"
  >
    <i class="fa fa-info-circle" />
    <popper
      :options="popperOptions"
      :visible="visible"
    >
      <span>
        My helpful content!
      </span>
    </popper>
  </a>
</template>

<script>
export default {
  data () {
    return {
      visible: false,
      popperOptions: {
        // instance-specific options
      }
    }
  },
  methods: {
    handleEnter () {
      this.visible = true
    },
    handleLeave () {
      setTimeout(() => {
        this.visible = false
      }, 200)
    }
  }
}
</script>
```

## license

Apache 2.0
